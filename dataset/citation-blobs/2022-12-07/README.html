<h1 id="software-heritage-software-citation-blob-dataset">Software
Heritage — Software Citation Blob Dataset</h1>
<p>Authors: Roberto Di Cosmo and Valentin Lorentz Contact:
roberto@dicosmo.org</p>
<h2 id="description">Description</h2>
<p>This dataset contains all “software citation files” extracted from a
snapshot of the <a href="https://www.softwareheritage.org">Software
Heritage</a> archive taken on 2022-12-07.</p>
<p>In this context, a <em>software citation file</em> is a unique file
content (or “blob”) that appeared in a software origin archived by
Software Heritage as a file whose name is <code>codemetat.json</code> or
<code>citation.cff</code>, two kinds of machine readable metadata files
used for describing software or citing it. The exact file name pattern
used to select the blobs contained in the dataset can be found in the
SQL query file <code>01-get-citation-swhids.sql</code>. Note that the
file name was not expected to be at the project root, because project
subdirectories may contain different software modules with different
citation information, and we wanted to include those too.</p>
<h2 id="format">Format</h2>
<p>The dataset is organized as follows:</p>
<ul>
<li><p><code>blobs.tar.zst</code>: a <a
href="http://facebook.github.io/zstd/">Zst</a>-compressed tarball
containing deduplicated citation blobs, one per file. The tarball
contains 23’452 blobs, for a total uncompressed size of 142MiB.</p>
<p>The blobs are organized in a sharded directory structure that
contains files named like
<code>blobs/86/24/8624bcdae55baeef00cd11d5dfcfa60f68710a02</code>,
where:</p>
<ul>
<li><p><code>blobs/</code> is the root directory containing all citation
blobs</p></li>
<li><p><code>ff4779adbace73349374b9fd5d77a42ae4ec66c2</code> is the SHA1
checksum of a specific citation blobs, a codemeta.json for rdflib in
this case. Each citation blob is ultimately named with its SHA1:</p>
<pre><code>$ head blobs/ff/47/ff4779adbace73349374b9fd5d77a42ae4ec66c2 
{
  &quot;@context&quot;: [
    &quot;https://doi.org/doi:10.5063/schema/codemeta-2.0&quot;,
    &quot;http://schema.org&quot;
  ],
  &quot;@type&quot;: &quot;SoftwareSourceCode&quot;,
  &quot;identifier&quot;: &quot;rdflib&quot;,
  &quot;description&quot;: &quot;A friendly and concise user interface for performing common [...]&quot;,
  &quot;name&quot;: &quot;rdflib: A high level wrapper around the &#39;redland&#39; package for common &#39;rdf&#39; applications &quot;,
  &quot;codeRepository&quot;: &quot;https://github.com/cboettig/rdflib&quot;,
$ sha1sum blobs/ff/47/ff4779adbace73349374b9fd5d77a42ae4ec66c2
ff4779adbace73349374b9fd5d77a42ae4ec66c2  blobs/ff/47/ff4779adbace73349374b9fd5d77a42ae4ec66c2</code></pre></li>
<li><p><code>ff</code> and <code>47</code> are, respectively, the first
and second group of two hex digits in the blob SHA1</p></li>
</ul></li>
<li><p><code>blobs.csv.zst</code> a <a
href="http://facebook.github.io/zstd/">Zst</a>-compressed <a
href="https://en.wikipedia.org/wiki/Comma-separated_values">CSV</a>
index of all the blobs in the dataset. Each line in the index (except
the first one, which contains column headers) describes a citation blob
and is in the format <code>SWHID,SHA1,NAME</code>, for example:</p>
<pre><code>  swh:1:cnt:6daebd857f6f6a98dd9288ef7b942283f7fa4f0e,ff4779adbace73349374b9fd5d77a42ae4ec66c2,&quot;codemeta.json&quot;
  swh:1:cnt:e69de29bb2d1d6434b8b29ae775ad8c2e48c5391,da39a3ee5e6b4b0d3255bfef95601890afd80709,&quot;codemeta.json&quot;
  swh:1:cnt:e69de29bb2d1d6434b8b29ae775ad8c2e48c5391,da39a3ee5e6b4b0d3255bfef95601890afd80709,&quot;citation.cff&quot;</code></pre>
<p>where:</p>
<ul>
<li><p><strong>SWHID:</strong> the <a
href="https://docs.softwareheritage.org/devel/swh-model/persistent-identifiers.html">Software
Heritage persistent identifier</a> of the blob. It can be used to
retrieve and cross-reference the citation blob via the Software Heritage
archive, e.g., at: <a
href="https://archive.softwareheritage.org/swh:1:cnt:6daebd857f6f6a98dd9288ef7b942283f7fa4f0e"
class="uri">https://archive.softwareheritage.org/swh:1:cnt:6daebd857f6f6a98dd9288ef7b942283f7fa4f0e</a></p></li>
<li><p><strong>SHA1:</strong> the blob SHA1, that can be used to
cross-reference blobs in the <code>blobs/</code> directory</p></li>
<li><p><strong>NAME:</strong> <em>a</em> file name given to the citation
blob in a given software origin. As the same citation blob can have
different names in different contexts, the index contain multiple
entries for the same blob with different names, as it is the case in the
example above ( swh:1:cnt:e69de29bb2d1d6434b8b29ae775ad8c2e48c5391 is
the SWHID of the empty file).</p></li>
</ul></li>
<li><p><code>blobs-fileinfo.csv.zst</code> a <a
href="http://facebook.github.io/zstd/">Zst</a>-compressed <a
href="https://en.wikipedia.org/wiki/Comma-separated_values">CSV</a>
mapping from blobs to basic file information in the format:
<code>SWHID,MIME_TYPE,ENCODING,LINE_COUNT,WORD_COUNT,SIZE</code>,
where:</p>
<ul>
<li><strong>SWHID:</strong> blob SWHID</li>
<li><strong>MIME_TYPE:</strong> blob MIME type, as detected by
[libmagic][libmagic]</li>
<li><strong>ENCODING:</strong> blob character encoding, as detected by
[libmagic][libmagic]</li>
<li><strong>LINE_COUNT:</strong> number of lines in the blob (only for
textual blobs with UTF8 encoding)</li>
<li><strong>WORD_COUNT:</strong> number of words in the blob (only for
textual blobs with UTF8 encoding)</li>
<li><strong>SIZE:</strong> blob size in bytes</li>
</ul></li>
<li><p><code>blobs-origins.csv.zst</code> a <a
href="http://facebook.github.io/zstd/">Zst</a>-compressed <a
href="https://en.wikipedia.org/wiki/Comma-separated_values">CSV</a>
mapping of where citation blobs come from. Each line in the index
associate a citation blob to one of its origins in the format
<code>SWHID,URL</code>, for example:</p>
<pre><code>  swh:1:cnt:6daebd857f6f6a98dd9288ef7b942283f7fa4f0e,https://github.com/denatahvildari/rdflib</code></pre>
<p>Note that a citation blob can come from many different places, only
an arbitrary (and somewhat random) one is listed in this mapping.</p>
<p>If no origin URL is found in the Software Heritage archive, then a
blank is used instead. This happens when they were either being loaded
when the dataset was generated, or the loader process crashed before
completing the blob’s origin’s ingestion.</p></li>
<li><p><code>blobs-nb-origins.csv.zst</code> a <a
href="http://facebook.github.io/zstd/">Zst</a>-compressed <a
href="https://en.wikipedia.org/wiki/Comma-separated_values">CSV</a>
mapping of how many origins of this blob are known to Software Heritage.
Each line in the index associate a citation blob to this count in the
format <code>SWHID,NUMBER</code>, for example:</p>
<pre><code>  swh:1:cnt:6daebd857f6f6a98dd9288ef7b942283f7fa4f0e,9</code></pre></li>
<li><p><code>blobs-earliest.csv.zst</code> a <a
href="http://facebook.github.io/zstd/">Zst</a>-compressed <a
href="https://en.wikipedia.org/wiki/Comma-separated_values">CSV</a>
mapping from blobs to information about their (earliest) known
occurence(s) in the archive. Format:
<code>SWHID,EARLIEST_SWHID,EARLIEST_TS,OCCURRENCES</code>, where:</p>
<ul>
<li><strong>SWHID:</strong> blob SWHID</li>
<li><strong>EARLIEST_SWHID:</strong> SWHID of the earliest known commit
containing the blob</li>
<li><strong>EARLIEST_TS:</strong> timestamp of the earliest known commit
containing the blob, as a [Unix time][unixtime] integer</li>
<li><strong>OCCURRENCES:</strong> number of known commits containing the
blob</li>
</ul></li>
</ul>
<h2 id="changes-from-the-2022-04-25-dataset">Changes from the 2022-04-25
dataset</h2>
<ul>
<li>More input data, due to the SWH archive growing: more origins in
supported forges and package managers; and support for more forges and
package managers. See the <a
href="https://docs.softwareheritage.org/devel/archive-changelog.html">SWH
Archive Changelog</a> for details.</li>
</ul>
<h2 id="erratum">Erratum</h2>
<ul>
<li>Due to <a
href="https://gitlab.softwareheritage.org/swh/devel/swh-graph/-/issues/4788">a
bug in the swh-graph export</a>, timestamps in
<code>blobs-earliest.csv.zst</code> are shifted 1 or 2 hours back based
on the Europe/Paris timezone.</li>
</ul>
