<h1 id="software-heritage-license-blob-dataset">Software Heritage —
License Blob Dataset</h1>
<p>Author: Stefano Zacchiroli<br />
Contact: zack@upsilon.cc</p>
<h2 id="description">Description</h2>
<p>This dataset contains all “license files” extracted from a snapshot
of the <a href="https://www.softwareheritage.org">Software Heritage</a>
archive taken on 2024-12-06. (A long-term archival version of this
dataset is also <a
href="https://doi.org/10.5281/zenodo.6379164">available from
Zenodo</a>).</p>
<p>In this context, a <em>license file</em> is a unique file content (or
“blob”) that appeared in a software origin archived by Software Heritage
as a file whose name is often used to ship licenses in software
projects. Some name examples are: <code>COPYING</code>,
<code>LICENSE</code>, <code>NOTICE</code>, <code>COPYRIGHT</code>, etc.
The exact file name pattern used to select the blobs contained in the
dataset can be found in the <a
href="https://archive.softwareheritage.org/swh:1:cnt:7c6143bccbb0ff6604417b723f38bee06db0138b;origin=https://gitlab.softwareheritage.org/swh/devel/swh-graph;visit=swh:1:snp:d34d87373bb367ba310002693cb7c4c139c3b882;anchor=swh:1:rev:985dcf705e03fde55285ca8aaff2488f43e9a55f;path=/swh/graph/luigi/blobs_datasets.py;lines=128-131">SQL
query</a>. Note that the file name was not expected to be at the project
root, because project subdirectories can contain different licenses than
the top-level one, and we wanted to include those too.</p>
<h2 id="format">Format</h2>
<p>The dataset is organized as follows:</p>
<ul>
<li><p><code>blobs.tar.zst</code>: a <a
href="http://facebook.github.io/zstd/">Zst</a>-compressed tarball
containing deduplicated license blobs, one per file. The tarball
contains 10’951’035 blobs, for a total uncompressed size on disk of 122
GiB.</p>
<p>The blobs are organized in a sharded directory structure that
contains files named like
<code>blobs/86/24/8624bcdae55baeef00cd11d5dfcfa60f68710a02</code>,
where:</p>
<ul>
<li><p><code>blobs/</code> is the root directory containing all license
blobs</p></li>
<li><p><code>8624bcdae55baeef00cd11d5dfcfa60f68710a02</code> is the SHA1
checksum of a specific license blobs, a copy of the GPL3 license in this
case. Each license blob is ultimately named with its SHA1:</p>
<pre><code>$ head -n 3 blobs/86/24/8624bcdae55baeef00cd11d5dfcfa60f68710a02
                    GNU GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

$ sha1sum blobs/86/24/8624bcdae55baeef00cd11d5dfcfa60f68710a02
8624bcdae55baeef00cd11d5dfcfa60f68710a02  blobs/86/24/8624bcdae55baeef00cd11d5dfcfa60f68710a02</code></pre></li>
<li><p><code>86</code> and <code>24</code> are, respectively, the first
and second group of two hex digits in the blob SHA1</p></li>
</ul></li>
<li><p><code>blobs-sample20k.tar.zst</code>: analogous to
<code>blobs.tar.zst</code>, but containing “only” 20’000 randomly
selected license blobs</p></li>
<li><p><code>license-blobs.csv.zst</code> a <a
href="http://facebook.github.io/zstd/">Zst</a>-compressed <a
href="https://en.wikipedia.org/wiki/Comma-separated_values">CSV</a>
index of all the blobs in the dataset. Each line in the index (except
the first one, which contains column headers) describes a license blob
and is in the format <code>SWHID,SHA1,NAME</code>, for example:</p>
<pre><code>  swh:1:cnt:94a9ed024d3859793618152ea559a168bbcbb5e2,8624bcdae55baeef00cd11d5dfcfa60f68710a02,&quot;COPYING&quot;
  swh:1:cnt:94a9ed024d3859793618152ea559a168bbcbb5e2,8624bcdae55baeef00cd11d5dfcfa60f68710a02,&quot;COPYING.GPL3&quot;
  swh:1:cnt:94a9ed024d3859793618152ea559a168bbcbb5e2,8624bcdae55baeef00cd11d5dfcfa60f68710a02,&quot;COPYING.GLP-3&quot;</code></pre>
<p>where:</p>
<ul>
<li><p><strong>SWHID:</strong> the <a
href="https://docs.softwareheritage.org/devel/swh-model/persistent-identifiers.html">Software
Heritage persistent identifier</a> of the blob. It can be used to
retrieve and cross-reference the license blob via the Software Heritage
archive, e.g., at: <a
href="https://archive.softwareheritage.org/swh:1:cnt:94a9ed024d3859793618152ea559a168bbcbb5e2"
class="uri">https://archive.softwareheritage.org/swh:1:cnt:94a9ed024d3859793618152ea559a168bbcbb5e2</a></p></li>
<li><p><strong>SHA1:</strong> the blob SHA1, that can be used to
cross-reference blobs in the <code>blobs/</code> directory. May be empty
in the rare case where the blob is not present in the archive (eg.
because it was too large).</p></li>
<li><p><strong>NAME:</strong> <em>a</em> file name given to the license
blob in a given software origin. As the same license blob can have
different names in different contexts, the index contain multiple
entries for the same blob with different names, as it is the case in the
example above (yes, one of those has a typo in it, but it’s an
<em>original</em> typo from some repository!).</p></li>
</ul></li>
<li><p><code>blobs-fileinfo.csv.zst</code> a <a
href="http://facebook.github.io/zstd/">Zst</a>-compressed <a
href="https://en.wikipedia.org/wiki/Comma-separated_values">CSV</a>
mapping from blobs to basic file information in the format:
<code>sha1,mime_type,encoding,line_count,word_count,size</code>,
where:</p>
<ul>
<li><strong>sha1:</strong> blob SHA1</li>
<li><strong>mime_type:</strong> blob MIME type, as detected by <a
href="https://pubs.opengroup.org/onlinepubs/9699919799/utilities/file.html">libmagic</a></li>
<li><strong>encoding:</strong> blob character encoding, as detected by
<a
href="https://pubs.opengroup.org/onlinepubs/9699919799/utilities/file.html">libmagic</a></li>
<li><strong>line_count:</strong> number of lines in the blob (only for
textual blobs with UTF8 encoding)</li>
<li><strong>word_count:</strong> number of words in the blob (only for
textual blobs with UTF8 encoding)</li>
<li><strong>size:</strong> blob size in bytes</li>
</ul></li>
<li><p><code>blobs-scancode.csv.zst</code> a <a
href="http://facebook.github.io/zstd/">Zst</a>-compressed <a
href="https://en.wikipedia.org/wiki/Comma-separated_values">CSV</a>
mapping from blobs to software license detected in them by <a
href="https://www.aboutcode.org/projects/scancode.html">ScanCode</a>, in
the format: <code>sha1,license,score</code>, where:</p>
<ul>
<li><strong>sha1:</strong> blob SHA1</li>
<li><strong>license:</strong> license detected in the blob, as an <a
href="https://spdx.dev/">SPDX</a> identifier (or <a
href="https://scancode-licensedb.aboutcode.org/">ScanCode identifier</a>
for non-SPDX-indexed licenses)</li>
<li><strong>score:</strong> confidence score in the result, as a decimal
number between 0 and 100</li>
</ul>
<p>There may be zero or arbitrarily many lines for each blob.</p></li>
<li><p><code>blobs-scancode.ndjson.zst</code> a <a
href="http://facebook.github.io/zstd/">Zst</a>-compressed <a
href="http://ndjson.org/">line-delimited JSON</a>, containing a superset
of the information in <code>blobs-scancode.csv.zst</code>. Each line is
a JSON dictionary with three keys:</p>
<ul>
<li><strong>swhid</strong>: blob SWHID</li>
<li><strong>licenses</strong>: output of
<code>scancode.api.get_licenses(..., min_score=0)</code></li>
<li><strong>copyrights</strong>: output of
<code>scancode.api.get_copyrights(...)</code></li>
</ul>
<p>There is exactly one line for each blob. <code>licenses</code> and
<code>copyrights</code> keys are omitted for files not detected as plain
text.</p></li>
<li><p><code>blobs-origins.csv.zst</code> a <a
href="http://facebook.github.io/zstd/">Zst</a>-compressed <a
href="https://en.wikipedia.org/wiki/Comma-separated_values">CSV</a>
mapping of where license blobs come from. Each line in the index
associate a license blob to one of its origins in the format
<code>swhid,url</code>, for example:</p>
<pre><code>  swh:1:cnt:94a9ed024d3859793618152ea559a168bbcbb5e2,https://github.com/pombreda/Artemis</code></pre>
<p>Note that a license blob can come from many different places, only an
arbitrary (and somewhat random) one is listed in this mapping.</p>
<p>If no origin URL is found in the Software Heritage archive, then a
blank is used instead. This happens when they were either being loaded
when the dataset was generated, or the loader process crashed before
completing the blob’s origin’s ingestion.</p></li>
<li><p><code>blobs-nb-origins.csv.zst</code> a <a
href="http://facebook.github.io/zstd/">Zst</a>-compressed <a
href="https://en.wikipedia.org/wiki/Comma-separated_values">CSV</a>
mapping of how many origins of this blob are known to Software Heritage.
Each line in the index associate a license blob to this count in the
format <code>SWHID,NUMBER</code>, for example:</p>
<pre><code>  swh:1:cnt:94a9ed024d3859793618152ea559a168bbcbb5e2,2822260</code></pre></li>
<li><p><code>blobs-earliest.csv.zst</code> a <a
href="http://facebook.github.io/zstd/">Zst</a>-compressed <a
href="https://en.wikipedia.org/wiki/Comma-separated_values">CSV</a>
mapping from blobs to information about their (earliest) known
occurence(s) in the archive. Format:
<code>SWHID,EARLIEST_SWHID,EARLIEST_TS,OCCURRENCES</code>, where:</p>
<ul>
<li><strong>SWHID:</strong> blob SWHID</li>
<li><strong>EARLIEST_SWHID:</strong> SWHID of the earliest known commit
containing the blob</li>
<li><strong>EARLIEST_TS:</strong> timestamp of the earliest known commit
containing the blob, as a <a
href="https://en.wikipedia.org/wiki/Unix_time">Unix time</a>
integer</li>
<li><strong>OCCURRENCES:</strong> number of known commits containing the
blob</li>
</ul></li>
<li><p><code>licenses-annotated-sample.tar.gz</code>: ground truth,
i.e., manually annotated random sample of license blobs, with details
about the kind of information they contain.</p></li>
</ul>
<h2 id="changes-from-the-2022-12-07-dataset">Changes from the 2022-12-07
dataset</h2>
<ul>
<li><p>More input data, due to the SWH archive growing: more origins in
supported forges and package managers; and support for more forges and
package managers. See the <a
href="https://docs.softwareheritage.org/devel/archive-changelog.html">SWH
Archive Changelog</a> for details.</p></li>
<li><p>32 contents referenced by directories but missing from the
archive (eg. because they are too big) are now included in
<code>license-blobs.csv.zst</code> (with an empty SHA1),
<code>blobs-origins.csv.zst</code>,
<code>blobs-nb-origins.csv.zst</code>, and
<code>blobs-earliest.csv.zst</code>. They are not included in other
files, as that would require access to their content.</p></li>
</ul>
<h2 id="citation">Citation</h2>
<p>If you use this dataset for research purposes, please acknowledge its
use by citing the following paper:</p>
<ul>
<li>[<a href="https://arxiv.org/pdf/2204.00256.pdf">pdf</a>, <a
href="https://dblp.uni-trier.de/rec/conf/msr/Zacchiroli22.html?view=bibtex">bib</a>]
Stefano Zacchiroli. <a
href="https://doi.org/10.1145/3524842.3528491"><em>A Large-scale Dataset
of (Open Source) License Text Variants</em></a>. In proceedings of the
<a href="https://conf.researchr.org/home/msr-2022">2022 Mining Software
Repositories Conference (MSR 2022)</a>. 23-24 May 2022 Pittsburgh,
Pennsylvania, United States. ACM 2022.</li>
</ul>
<h2 id="references">References</h2>
<p>The dataset has been built using primarily the data sources described
in the following papers:</p>
<ul>
<li><p>[<a href="https://phaidra.univie.ac.at/open/o:931064">pdf</a>, <a
href="https://dblp.uni-trier.de/rec/conf/ipres/CosmoZ17.html?view=bibtex">bib</a>]
Roberto Di Cosmo, Stefano Zacchiroli. <a
href="https://hdl.handle.net/11353/10.931064">Software Heritage: Why and
How to Preserve Software Source Code</a>. In Proceedings of iPRES 2017:
14th International Conference on Digital Preservation, Kyoto, Japan,
25-29 September 2017.</p></li>
<li><p>[<a
href="https://upsilon.cc/~zack/research/publications/msr-2019-swh.pdf">pdf</a>,
<a
href="https://dblp.uni-trier.de/rec/conf/msr/PietriSZ19.html?view=bibtex">bib</a>]
Antoine Pietri, Diomidis Spinellis, Stefano Zacchiroli. <a
href="https://doi.org/10.1109/MSR.2019.00030">The Software Heritage
Graph Dataset: Public software development under one roof</a>. In
proceedings of MSR 2019: The 16th International Conference on Mining
Software Repositories, May 2019, Montreal, Canada. Pages 138-142, IEEE
2019.</p></li>
</ul>
