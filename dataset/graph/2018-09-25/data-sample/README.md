## Loading the data sample

The data sample can be loaded in PostgreSQL using:

    createdb swh-data-sample;
    psql swh-data-sample < swh_import.sql
